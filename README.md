Ansible Role to manage InfluxDB installation
-------------------------------------------

This simple role allow you to install InfluxDB on a specific machine.

## Example

```ansible-playbook
- name: Install InfluxDB
  include_role:
    name: influxdb
```
